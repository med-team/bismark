Source: bismark
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Liubov Chuprikova <chuprikovalv@gmail.com>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/med-team/bismark
Vcs-Git: https://salsa.debian.org/med-team/bismark.git
Homepage: https://www.bioinformatics.babraham.ac.uk/projects/bismark/
Rules-Requires-Root: no

Package: bismark
Architecture: all
Depends: ${shlibs:Depends},
         ${misc:Depends},
         ${perl:Depends},
         samtools,
         bowtie2,
         hisat2
Description: bisulfite read mapper and methylation caller
 Bismark is a program to map bisulfite treated sequencing reads to a
 genome of interest and perform methylation calls in a single step. The
 output can be easily imported into a genome viewer, such as SeqMonk, and
 enables a researcher to analyse the methylation levels of their samples
 straight away. It's main features are:
 .
  * Bisulfite mapping and methylation calling in one single step
  * Supports single-end and paired-end read alignments
  * Supports ungapped and gapped alignments
  * Alignment seed length, number of mismatches etc. are adjustable
  * Output discriminates between cytosine methylation in CpG, CHG and
    CHH context
